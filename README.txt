Tactic theme for Drupal 8

Tactic is a theme based on 
Bootstrap, a sleek, intuitive, and powerful front-end framework 
for faster and easier web development.
Tactic focus on layout flexibility 
and customization using theme parameters.


Drupal Theme Features:
 - Modern & Clean Design
 - Responsive Layout
 - Drupal 8 Ready
 - Font Awesome Integrated
 - 5+ Social Networks
 - Add Copyright Text
 - Scroll to top option
 - Home page Text Slider

Installation

Unpack and upload Tactic Theme on /themes folder.