/* *
 * @file: Javascript
 */
jQuery(document).ready(function ($) {
  // Mobile menu.
  $('.mobile-menu').click(function () {
    $(this).next('.primary-menu-wrapper').toggleClass('active-menu');
  });
  $('.close-mobile-menu').click(function () {
    $(this).closest('.primary-menu-wrapper').toggleClass('active-menu');
  });

  // Full page search.
  $('.search-icon').click(function() {
    $('.search-box').toggleClass('open');
    return false;
  });

  $('.search-box-close').on('click', function() {
    $('.search-box').removeClass('open');
    return false;
  });

  // Full Screen header.
  function fullscreen() {
    var padding_top = 0;
    if ($('#toolbar-bar').length) {
      padding_top += $('#toolbar-bar').height();
    }
    if ($('.toolbar-tray-horizontal').length) {
      padding_top += $('.toolbar-tray-horizontal').height();
    }
    var sticky_header = $('.header-main').height();
    padding_top += sticky_header;
    $("body").css("paddingTop", padding_top);

  	$('.frontpage.homepage .header').css({
  		height: $(window).height() - sticky_header
  	});
    var headerheight = $('.header-main').outerHeight();
    $('.slider').css({ height: $(window).height() - sticky_header })
  }
  // Scroll To Top.
  $(window).scroll(function () {
    if ($(this).scrollTop() > 80) {
      $('.scrolltop').fadeIn('slow');
    } else {
      $('.scrolltop').fadeOut('slow');
    }
  });
  $('.scrolltop').click(function () {
    $('html, body').animate( { scrollTop: 0 }, 'slow');
  });

// End document ready.
  fullscreen();
  $(window).resize(function () {
    fullscreen();
  });
});

